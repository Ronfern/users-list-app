//
//  ServerManager.swift
//  UserListApp
//
//  Created by Роман Чугай on 10/22/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON

struct Constant {
  
    struct refsConstans {
        static let getUser = "https://randomuser.me/api/?results=15&seed=abc&page="
    }
}

class ServerManager {
  
  static let sharedInstance = ServerManager()
  private init() {}
  
  public func Serv(page: Int, completion: @escaping([User]) -> Void, failure: @escaping(Error) -> Void) {
    let url = Constant.refsConstans.getUser + "\(page)"
    Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON { (response) in
      guard response.result.isSuccess, let value = response.result.value else { return }
      let jsonObjects = JSON(value)["results"].array!.map { json -> User in
        User(dictionaryJSON: json)
      }
      completion(jsonObjects)
      guard case let .failure(error) = response.result else { return }
      failure(error)
    }
  }
  

}

