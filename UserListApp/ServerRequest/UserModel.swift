//
//  UserModel.swift
//  UserListApp
//
//  Created by Роман Чугай on 10/22/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
  
  var id: String?
  var firstName: String?
  var lastName: String?
  var phone: String?
  var email: String?
  var profileImageUrl: String?
  var profileImageUrlMedium: String?
  
  init(dictionaryJSON: JSON) {
    self.id = dictionaryJSON["id"]["value"].string
    self.firstName = dictionaryJSON["name"]["first"].string
    self.lastName = dictionaryJSON["name"]["last"].string
    self.profileImageUrl = dictionaryJSON["picture"]["thumbnail"].string
    self.profileImageUrlMedium = dictionaryJSON["picture"]["medium"].string
    self.phone = dictionaryJSON["phone"].string
    self.email = dictionaryJSON["email"].string
    if self.id == nil {
      self.id = UUID.init().uuidString
    }
  }
  
}



