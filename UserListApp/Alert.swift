//
//  Alert.swift
//  Architecture
//
//  Created by Роман Чугай on 1/25/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import UIKit
import Foundation

class UIAlertView: UIViewController, UIAlertViewDelegate {
  
  static let shared = UIAlertView()
  
  func showAlertWith(title: String, message: String, style: UIAlertController.Style = .alert) {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
    let action = UIAlertAction(title: "OK", style: .default) { (action) in
      self.dismiss(animated: true, completion: nil)
    }
    alertController.addAction(action)
    UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
  }
}

extension UIApplication {
  
  static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
    if let nav = base as? UINavigationController {
      return topViewController(base: nav.visibleViewController)
    }
    if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
      return topViewController(base: selected)
    }
    if let presented = base?.presentedViewController {
      return topViewController(base: presented)
    }
    
    return base
  }
}
