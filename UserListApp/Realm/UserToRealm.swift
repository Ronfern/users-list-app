//
//  UserToRealm.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/31/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import Foundation
import RealmSwift

class UserToRealm: Object {
  
  @objc dynamic var id = String()
  @objc dynamic var firstName = String()
  @objc dynamic var lastName = String()
  @objc dynamic var phone = String()
  @objc dynamic var email = String()
  @objc dynamic var profileImage = Data()
  
  override static func primaryKey() -> String? {
    return "id"
  }
}

// MARK: - CRUD methods

extension UserToRealm {
  
  static func all(in realm: Realm = RealmStorage.sharedInstance.uiRealm) -> Results<UserToRealm> {
    return realm.objects(UserToRealm.self)
  }
  
  func writeToRealm() {
    let writeRealm = RealmStorage.sharedInstance.uiRealm
    do {
      try writeRealm.write {
        writeRealm.add(self, update: true)
      }
    } catch let error {
      print("ERROR DELETING : \(error)")
    }
  }
  
  func delete() {
    let writeRealm = RealmStorage.sharedInstance.uiRealm
    do {
      try writeRealm.write {
        writeRealm.delete(self)
      }
    } catch let error {
      print("ERROR DELETING : \(error)")
    }
  }
  
}
