//
//  TableUserCell.swift
//  UserListApp
//
//  Created by Роман Чугай on 10/29/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import UIKit
import PhoneNumberKit

class TableUserCell: UITableViewCell {
  
  @IBOutlet weak var profileImage: UIImageView!
  @IBOutlet weak var fullNameUser: UILabel!
  @IBOutlet weak var numberUser: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    profileImage!.layer.borderWidth = 1
    profileImage!.layer.masksToBounds = false
    profileImage!.layer.borderColor = UIColor.clear.cgColor
    profileImage!.layer.cornerRadius = profileImage!.frame.height/2
    profileImage!.clipsToBounds = true
  }
  
  var user: User? {
    didSet {
      guard let checkUser = user else { return }
      numberUser.text = PartialFormatter().formatPartial(checkUser.phone!)
      fullNameUser.text = checkUser.firstName! + " " + checkUser.lastName!
      if checkUser.profileImageUrl?.isEmpty == false {
        setImageToUser(checkUser.profileImageUrl!)
      }
    }
  }
  
  fileprivate func setImageToUser (_ imageUrl: String) {
    if let url = URL(string: imageUrl) {
      self.profileImage!.af_setImage(withURL: url, placeholderImage: UIImage(named: "tab_saved"), filter: nil, runImageTransitionIfCached: true)
    }
  }
  
  var userToRealm: UserToRealm? {
    didSet {
      guard let checkUser = userToRealm else { return }
      numberUser.text = PartialFormatter().formatPartial(checkUser.phone)
      fullNameUser.text = checkUser.firstName + " " + checkUser.lastName
      profileImage.image = UIImage(data: checkUser.profileImage)
    }
  }
  
}
