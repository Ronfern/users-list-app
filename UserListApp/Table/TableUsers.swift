//
//  ViewController.swift
//  UserListApp
//
//  Created by Роман Чугай on 10/22/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import UIKit

class TableUsers: UITableViewController, SaveToRealm {
  
  @IBOutlet weak var tblList: UITableView!
  
  static let Cell = "Cell"
  var page = 1
  var usersArray: [User] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.topItem?.title = "Users"
    tblList.register(UINib.init(nibName: "TableUserCell", bundle: nil), forCellReuseIdentifier: TableUsers.Cell)
    tblList.tableFooterView = UIView(frame: .zero)
    serverRequest()
  }
  
  func serverRequest() {
    ServerManager.sharedInstance.Serv(page: page, completion: { (user) in
      self.usersArray = self.usersArray + user
      self.tblList.reloadData()
    }) { (error) in
      print(error)
    }
    page = page + 1
  }
  
  //MARK:- Delegation
  
  func saveFunction() {
    self.tabBarController?.selectedIndex = 1
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  func backFunction() {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  //MARK:- UITableViewMethods
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return usersArray.count 
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: TableUsers.Cell) as! TableUserCell
    let user = self.usersArray[indexPath.row]
    cell.user = user
    cell.selectionStyle = .none
    return cell
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60.0
  }
  
  override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if indexPath.row == usersArray.count - 1 {
      loadTable()
    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.performSegue(withIdentifier: "editUser", sender: self)
  }
  
  fileprivate func loadTable() {
    serverRequest()
  }
  
  //MARK:- Navigation to Edit controller
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "editUser" {
      let indexPath = tableView.indexPathForSelectedRow!
      if let editVC = segue.destination as? EditProfileController {
        let user = usersArray[indexPath.row]
        editVC.delegate = self
        editVC.user = user
      }
    }
  }
  
}


