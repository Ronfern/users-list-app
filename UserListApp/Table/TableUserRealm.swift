//
//  UserTableRealm.swift
//  UserListApp
//
//  Created by Роман Чугай on 10/24/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import UIKit
import RealmSwift


class TableUserRealm: UITableViewController, SaveToRealm, DatasourceDelegate {
 
  @IBOutlet weak var tblList: UITableView!
  private lazy var controller: TableRealmController = TableRealmController(controller: self)
  private var selectedUser: UserToRealm?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.topItem?.title = "Saved"
    controller.viewDidLoad()
  }
  
  //MARK:- Delegation
  
  func saveFunction() {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  func backFunction() {
    self.tabBarController?.selectedIndex = 0
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  func didSelectUserGoToEditMenu(_ user: UserToRealm) {
    selectedUser = user
    performSegue(withIdentifier: "editUserRealm", sender: self)
  }
  
  //MARK:- Navigation to Edit controller

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let editVC = segue.destination as? EditProfileController {
      editVC.delegate = self
      editVC.userRealm = selectedUser
    }
  }

}


