//
//  Result.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/22/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import Foundation

enum CheckedTextFieldResult {
  case succes
  case error(String)
}
