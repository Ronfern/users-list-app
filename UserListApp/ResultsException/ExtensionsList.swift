//
//  ExtensionsList.swift
//  UserListApp
//
//  Created by Роман Чугай on 11/29/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import Foundation
import UIKit

extension IndexPath {
  static func fromRow(_ row: Int) -> IndexPath {
    return IndexPath(row: row, section: 0)
  }
}

extension UITableView {
  func applyChanges(deletions: [Int], insertions: [Int], updates: [Int]) {
    beginUpdates()
    deleteRows(at: deletions.map(IndexPath.fromRow), with: .automatic)
    insertRows(at: insertions.map(IndexPath.fromRow), with: .automatic)
    reloadRows(at: updates.map(IndexPath.fromRow), with: .automatic)
    endUpdates()
  }
}

extension UITextField {
  func checkText() -> String {
    if self.text?.isEmpty == true {
      return self.placeholder!
    } else {
      return self.text!
    }
  }
}

//private var maxLengths = [UITextField: Int]()

//extension UITextField {
//
//  @IBInspectable var maxLength: Int {
//    get {
//      guard let length = maxLengths[self] else {
//        return Int.max
//      }
//
//      return length
//    }
//    set {
//      maxLengths[self] = newValue
//      addTarget(self, action: #selector(limitLength), for: .editingChanged)
//    }
//  }
//
//  @objc func limitLength(textField: UITextField) {
//    guard let prospectiveText = textField.text, prospectiveText.count > maxLength else {
//      return
//    }
//
//    let selection = selectedTextRange
//    let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
//
//    #if swift(>=4.0)
//    text = String(prospectiveText[..<maxCharIndex])
//    #else
//    text = prospectiveText.substring(to: maxCharIndex)
//    #endif
//
//    selectedTextRange = selection
//  }
//
//}
