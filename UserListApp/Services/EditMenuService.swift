//
//  EditMenuService.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/21/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import UIKit

protocol EditMenuService {
  func checkInformation(dataFromEditMenu: (id: String, firstName: String, lastName: String, phone: String, email: String, image: UIImage?), completion: @escaping (CheckedTextFieldResult) -> Void)
}
