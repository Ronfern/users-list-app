//
//  EditProfilePhoto.swift
//  UserListApp
//
//  Created by Роман Чугай on 10/24/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import AlamofireImage
import KUIActionSheet
import ValidationComponents

extension EditProfileController: UIImagePickerControllerDelegate {
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    guard let selectedImage = info[.originalImage] as? UIImage else {
      fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
    }
    let resizedImage = selectedImage.af_imageScaled(to: profileImage.frame.size)
    profileImage.image = resizedImage
    dismiss(animated: true, completion: nil)
  }
  
  func handleSelectProfileImageView() {
    picker.sourceType = .photoLibrary
    picker.allowsEditing = false
    present(picker, animated: true, completion: nil)
  }
  
  func openCamera() {
    guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
      print("This device doesn't have a camera.")
      return
    }
    picker.sourceType = .camera
    picker.cameraDevice = .rear
    picker.allowsEditing = false
    present(picker, animated: true)
  }
  
  @objc func keyboardShow() {
    self.view.frame.origin.y = 0
    if UIDevice().userInterfaceIdiom == .phone {
      switch UIScreen.main.nativeBounds.height {
      case 1136: self.view.frame.origin.y -= 192
      print("iPhone 5 or 5S or 5C")
      case 1334: self.view.frame.origin.y -= 188
      print("iPhone 6/6S/7/8")
      case 2208: self.view.frame.origin.y -= 190
      print("iPhone 6+/6S+/7+/8+")
      case 2436: self.view.frame.origin.y -= 180
      print("iPhone X")
      default: self.view.frame.origin.y -= 180
      print("unknown")
      }
    }
  }
  
  @objc func keyboardHide() {
    self.view.frame.origin.y = 0
  }
  
  @objc func doneButtonAction() {
    self.view.endEditing(true)
  }
  
}

extension EditProfileController: UINavigationControllerDelegate {
  
  func installElements() {
    tableView.tableFooterView = UIView(frame: .zero)
    picker.delegate = self
    profileImage!.layer.borderWidth = 1
    profileImage!.layer.masksToBounds = false
    profileImage!.layer.borderColor = UIColor.clear.cgColor
    profileImage!.layer.cornerRadius = profileImage!.frame.height/2
    profileImage!.clipsToBounds = true
    NotificationCenter.default.addObserver(self,selector: #selector(keyboardShow),
                                           name: UIResponder.keyboardWillShowNotification,
                                           object: nil)
    
    NotificationCenter.default.addObserver(self,selector: #selector(keyboardHide),
                                           name: UIResponder.keyboardWillHideNotification,
                                           object: nil)
  }
  
  @objc func library() {
    let actionSheet = KUIActionSheet.view(parentViewController: self)
    actionSheet?.cancelButton.setTitle("Cancel", for: .normal)
    actionSheet?.cancelButton.setTitleColor(UIColor.red, for: [])
    actionSheet?.add(item: KUIActionSheetItem(title: "Open camera", destructive: false) { (item) in
      self.openCamera()
    })
    actionSheet?.add(item: KUIActionSheetItem(title: "Gallery", destructive: false) { (item) in
      self.handleSelectProfileImageView()
    })
    actionSheet?.show()
  }

}

extension EditProfileController: UITextFieldDelegate {

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
}
