//
//  ServiceImplimentation.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/21/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import Foundation
import ValidationComponents
import PhoneNumberKit

class ServiceImplimentation: EditMenuService {
  
  func checkInformation(dataFromEditMenu: (id: String, firstName: String, lastName: String, phone: String, email: String, image: UIImage?), completion: @escaping (CheckedTextFieldResult) -> Void) {
    
    guard self.isValidEmail(email: dataFromEditMenu.email) else {
      completion(.error("Email is not valid"))
      return
    }
    guard self.isValidFirstAndLastName(checkName: dataFromEditMenu.firstName) else {
      completion(.error("First name is not valid"))
      return
    }
    guard self.isValidFirstAndLastName(checkName: dataFromEditMenu.lastName) else {
      completion(.error("Last name is not valid"))
      return
    }
    guard self.isValidPhone(phone: dataFromEditMenu.phone) else {
      completion(.error("Phone is not valid"))
      return
    }
    guard isEmhtyImage(image: dataFromEditMenu.image) else {
      completion(.error("Select a photo"))
      return
    }
    completion(.succes)
  }
  
  fileprivate func isEmhtyImage(image: UIImage?) -> Bool {
    return image != nil
  }
  
 fileprivate func isValidEmail(email: String) -> Bool {
    let predicate = EmailValidationPredicate()
    return  predicate.evaluate(with: email)
  }
  
  fileprivate func isValidPhone(phone: String) -> Bool {
    if phone.count > 9 && phone.count < 16 {
      return true
    } else {
      return false
    }
  }
  
 fileprivate func isValidFirstAndLastName(checkName: String) -> Bool {
    let fullNameRegEx = "\\w{2,18}"
    let fullNameTest = NSPredicate(format:"SELF MATCHES %@", fullNameRegEx)
    return fullNameTest.evaluate(with: checkName)
  }
  
}
