//
//  DataProvidable.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/21/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import Foundation
import RealmSwift

protocol Dataprovidable {
  func numberOfRows() -> Int
  func getUserFromRealm(by indexPath: IndexPath) -> UserToRealm
  func installNewUser(user: User, checkedData: (id: String, firstName: String, lastName: String, phone: String, email: String, image: UIImage?), completion: @escaping () -> Void)
  func editExistUser(user: UserToRealm, checkedData: (id: String, firstName: String, lastName: String, phone: String, email: String, image: UIImage?), completion: @escaping () -> Void)
}

class DataProvider: Dataprovidable {

  var models = UserToRealm.all()
  
  func installNewUser(user: User, checkedData: (id: String, firstName: String, lastName: String, phone: String, email: String, image: UIImage?), completion: @escaping () -> Void) {
    DispatchQueue.main.async {
      let userToRealm = UserToRealm()
      userToRealm.id = checkedData.id
      userToRealm.firstName = checkedData.firstName
      userToRealm.lastName = checkedData.lastName
      userToRealm.phone = checkedData.phone
      userToRealm.email = checkedData.email
      userToRealm.profileImage = checkedData.image!.pngData()!
      userToRealm.writeToRealm()
      completion()
    }
  }
  
  func editExistUser(user: UserToRealm, checkedData: (id: String, firstName: String, lastName: String, phone: String, email: String, image: UIImage?), completion: @escaping () -> Void) {
    DispatchQueue.main.async {
      do {
        let writeRealm = RealmStorage.sharedInstance.uiRealm
        try writeRealm.write {
          user.firstName = checkedData.firstName
          user.lastName = checkedData.lastName
          user.phone = checkedData.phone
          user.email = checkedData.email
          user.profileImage = checkedData.image!.pngData()!
          completion()
        }
      } catch let error {
        print("ERROR: \(error)")
      }
    }
  }
  
  func numberOfRows() -> Int {
    return models.count
  }
  
  func getUserFromRealm(by indexPath: IndexPath) -> UserToRealm {
    return models[indexPath.row]
  }
  
}
