//
//  RealmTableDelegate.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/22/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import UIKit
import RealmSwift

protocol DatasourceDelegate: class{
  func didSelectUserGoToEditMenu(_ user: UserToRealm)
}

class RealmTableDelegate: NSObject, UITableViewDelegate {

  weak var delegate: DatasourceDelegate?
  var dataProvider: Dataprovidable
 
  init(dataProvider: Dataprovidable) {
    self.dataProvider = dataProvider
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let user = dataProvider.getUserFromRealm(by: indexPath)
    delegate?.didSelectUserGoToEditMenu(user)
  }
}
  


