//
//  TableRealmController.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/21/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//
import UIKit
import RealmSwift

class TableRealmController: NSObject {

  weak var tableController: TableUserRealm?
  private let dataProvider = DataProvider()
  private lazy var dataSource = RealmTableDataSource(dataProvider: dataProvider)
  private lazy var tableDelgate = RealmTableDelegate(dataProvider: dataProvider)
  private var itemsToken: NotificationToken?
  
  init(controller: TableUserRealm) {
    self.tableController = controller
  }
  
  var tableView: UITableView? {
    return tableController?.tableView
  }
  
  func delegating() {
    tableView?.register(UINib.init(nibName: "TableUserCell", bundle: nil), forCellReuseIdentifier: TableUsers.Cell)
    tableView?.tableFooterView = UIView(frame: .zero)
    tableView?.delegate = tableDelgate
    tableView?.dataSource = dataSource
    tableDelgate.delegate = tableController
  }
  
  func observeRealmDatabase() {
    itemsToken = dataProvider.models.observe { [weak tableView] changes in
      guard let tableView = tableView else { return }
      switch changes {
      case .initial:
        tableView.reloadData()
      case .update(_, let deletions, let insertions, let updates):
        tableView.applyChanges(deletions: deletions, insertions: insertions, updates: updates)
      case .error: break
      }
    }
  }
  
  func viewDidLoad() {
    delegating()
    observeRealmDatabase()
  }
  
}





