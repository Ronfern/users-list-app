//
//  RealmTableDataSource.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/21/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import UIKit
import RealmSwift

class RealmTableDataSource: NSObject, UITableViewDataSource {
  
  static let Cell = "Cell"
  var dataProvider: Dataprovidable
  
  init(dataProvider: Dataprovidable) {
    self.dataProvider = dataProvider
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataProvider.numberOfRows()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: RealmTableDataSource.Cell) as! TableUserCell
    let user = dataProvider.getUserFromRealm(by: indexPath)
    cell.userToRealm = user
    cell.selectionStyle = .none
    return cell
  }
  

  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    let editingRow = dataProvider.getUserFromRealm(by: indexPath)
    deleteItem(item: editingRow)
  }
  
  fileprivate func deleteItem(item: UserToRealm) {
    item.delete()
  }
  
}


