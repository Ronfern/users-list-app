//
//  Interactor.swift
//  UserListApp
//
//  Created by Роман Чугай on 1/21/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

class Interactor {
  
  private let examination: EditMenuService = ServiceImplimentation()
  
  var dataProvider: Dataprovidable?
  
  init(dataProvider: DataProvider) {
    self.dataProvider = dataProvider
  }
  
  func createUser(_ user: EditProfileController) {
    let information = (id: user.user!.id!, firstName: user.firstName.checkText(), lastName: user.lastName.checkText(), phone: user.phone.checkText(), email: user.email.checkText(),image: user.profileImage.image)
    examination.checkInformation(dataFromEditMenu: information) { (result) in
      switch result {
      case .succes:
        self.dataProvider?.installNewUser(user: user.user!, checkedData: information, completion: {
           user.delegate?.saveFunction()
        })
      case .error(let error):
        UIAlertView.shared.showAlertWith(title: "Error", message: error)
      }
    }
  }
  
  func editExistUser(_ user: EditProfileController) {
    let information = (id: user.userRealm!.id, firstName: user.firstName.checkText(), lastName: user.lastName.checkText(), phone: user.phone.checkText(), email: user.email.checkText(), image: user.profileImage.image)
    examination.checkInformation(dataFromEditMenu: information) { (result) in
      switch result {
      case .succes:
        self.dataProvider?.editExistUser(user: user.userRealm!, checkedData: information, completion: {
          user.delegate?.saveFunction()
        })
      case .error(let error):
        UIAlertView.shared.showAlertWith(title: "Error", message: error)
      }
    }
  }
  
}
