//
//  EditProfileController.swift
//  UserListApp
//
//  Created by Роман Чугай on 10/23/18.
//  Copyright © 2018 Роман Чугай. All rights reserved.
//

import UIKit
import Foundation
import PhoneNumberKit

class EditProfileController: UITableViewController {
  
  @IBOutlet weak var profileImage: UIImageView!
  @IBOutlet weak var firstName: UITextField!
  @IBOutlet weak var lastName: UITextField!
  @IBOutlet weak var email: UITextField!
  @IBOutlet weak var phone: PhoneNumberTextField!
  
  private let dataProvider = DataProvider()
  private lazy var examinationService = Interactor(dataProvider: dataProvider)
  
  weak var delegate: SaveToRealm?
  let picker = UIImagePickerController()
  var userImage: UIImage?
  var user: User?
  var userRealm: UserToRealm?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    installElements()
    
    if let user = user {
      installUser(user)
    } else {
      installUserFromRealm(userRealm!)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.isNavigationBarHidden = false
    let right = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveAction))
    navigationItem.rightBarButtonItem = right
    let left = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action:  #selector(backAction))
    navigationItem.leftBarButtonItem = left
    //
    let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
    let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
    toolbar.setItems([flexSpace, doneBtn], animated: false)
    toolbar.sizeToFit()
    self.phone.inputAccessoryView = toolbar
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    NotificationCenter.default.removeObserver(self)
  }
  
  
  @IBAction func changePhotoAction(_ sender: Any) {
    library()
  }
  
  @objc func backAction() {
    delegate?.backFunction()
  }
  
  @objc func saveAction() {
    if user != nil {
      examinationService.createUser(self)
    } else {
      examinationService.editExistUser(self)
    }
  }
  
  func installUser(_ user: User) {
    firstName.placeholder = user.firstName
    lastName.placeholder = user.lastName
    email.placeholder = user.email
    phone.placeholder = user.phone
    if let url = URL(string: user.profileImageUrlMedium!) {
      self.profileImage!.af_setImage(withURL: url, placeholderImage: UIImage(named: "tab_saved"), filter: nil, runImageTransitionIfCached: true)
    }
  }
  
  func installUserFromRealm(_ user: UserToRealm) {
    firstName.placeholder = user.firstName
    lastName.placeholder = user.lastName
    email.placeholder = user.email
    phone.placeholder = user.phone
    self.profileImage.image = UIImage(data: user.profileImage)
  }
  
}




