//
//  EditNavigationProtocol.swift
//  UserListApp
//
//  Created by Роман Чугай on 2/4/19.
//  Copyright © 2019 Роман Чугай. All rights reserved.
//

import Foundation

protocol SaveToRealm: class {
  func saveFunction()
  func backFunction()
}
